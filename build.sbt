import sbt.KeyRanks.APlusSetting

val akkaVersion = SettingKey[String]("akka-version", "The version of Akka used for building.", APlusSetting)
val akkaHttpVersion = SettingKey[String]("akka-http-version", "The version of Akka-HTTP used for building.", APlusSetting)

lazy val commonSettings = Seq(
  name := "pj-celtra-backend",
  version := "1.0",
  scalaVersion := "2.12.2",
  akkaVersion := "2.5.2",
  akkaHttpVersion := "10.0.8"
)

lazy val cli = InputKey[Unit]("cli", "Run the CLI.")
fullRunInputTask(cli, Runtime, "com.naltharial.backend.BackendCLI")
logLevel in cli := sbt.Level.Warn

lazy val server = TaskKey[Unit]("server", "Run the server.")
fullRunTask(server, Runtime, "com.naltharial.backend.BackendAPI")
logLevel in server := sbt.Level.Warn

run := {
  println("Please use 'cli [<filters...>] [-a]' to run the application.")
}

lazy val root = (project in file("."))
  .settings(
    commonSettings,
    resolvers +=
      "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots",
    libraryDependencies ++= Seq(
      // akka
      "com.typesafe.akka" %% "akka-actor" % akkaVersion.value,
      // streams
      "com.typesafe.akka" %% "akka-stream" % akkaVersion.value,
      // akka http
      "com.typesafe.akka" %% "akka-http" % akkaHttpVersion.value,
      // JSON
      "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion.value,
      // DB
      "mysql" % "mysql-connector-java" % "6.0.6",
      "com.zaxxer" % "HikariCP" % "2.6.3",
      "com.newmotion" %% "akka-rabbitmq" % "4.0.0",
      // presentation
      "com.github.inamik.text.tables" % "inamik-text-tables" % "1.0-SNAPSHOT",
      // libs
      "com.github.nscala-time" %% "nscala-time" % "2.16.0",
      "org.rogach" %% "scallop" % "3.0.3",
      // test
      "org.scalatest" %% "scalatest" % "3.0.1" % "test",
      "com.typesafe.akka" %% "akka-testkit" % akkaVersion.value,
      "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpVersion.value % "test"
    )
  )
