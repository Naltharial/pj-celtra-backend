# PJCB #

PJCB is a platform that supports a RabbitMQ-based exchange of events coming in through the REST API endpoint.

## Prerequisites ##

* Java 8
* MySQL
* RabbitMQ

## Installation ##

* clone this repository
* (under *NIX) you may need to `chmod +x` scripts `sbt` and `sbt-dist/bin/sbt`
* create a normal and a test database on your MySQL server
    * `.sql` script to create the tables for the ***normal*** database is in `src/main/resources/db/migration/upgrade_v1_accounts_table.sql`
    * test database should remain empty, as it is used automatically during testing
* update the configuration in `src/main/resources/application.conf` with your access details

## Using PJCB ##

The application comes with two basic methods of running - the CLI/Integrated and the Server mode. While the latter serves only to start a REST API to receive input from, the former may be configured to do both the API and the CLI output. If not running as a server, the CLI may be started multiple times wherever desired, so long as it's able to connect to the RabbitMQ broker.

### Server ###

The server only supports a single mode, ran through `sbt server`, in which it listens to the configured endpoint and relays the messages to the broker.

### CLI ###

CLI can be started with a basic command of `sbt cli` in which it will simply relay all messages received from the broker. It can also be switched into *aggregate* and *integrated* modes, together with a support for filtering, for a total signature of `sbt "cli [-a] [-s] [<filter list ...>]"`.

* `sbt "cli 1 2 3 4"` will filter only the messages with IDs 1, 2, 3 and 4.
* `sbt "cli -a"` will switch to aggregate mode, in which the incoming messages will be displayed in a sum-total style in a table
* `sbt "cli -s"` is the integrated mode, which runs both the API and the CLI
* by combining the switches it is possible to run, `sbt "cli -as 1 2 3"`, which is an integrated mode that also displays an aggregation of messages with IDs 1, 2 or 3

### Testing ###

Unit tests can be run with the standard `sbt test` command. To test the performance specifically, a configuration is supplied in `apache-jmeter-benchmark.jmx` for the Apache JMeter.

## Architecture ##

The application is written in Scala, using the Akka library to perform most of the heavy tasks though AkkaActors and the AkkaHttp framework for the REST API. This design allows the application to be responsive and leverage Scala's concurrency for performance and scalability.

By using an external PubSub message broker like RabbitMQ it can achieve very high speeds while also not tying itself down to a single instance or machine - a CLI can access the message system from anywhere it can establish a connection to the broker. The database used for persistant storage is MySQL to achieve the reliablity of a classical relational database for the accounts.

The application is fault tolerant and should only need to wait for the server(s) to be accessible (after a brief reconnect delay) before being operational again.