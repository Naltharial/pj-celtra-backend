package com.naltharial.backend.mock

import scala.concurrent._
import ExecutionContext.Implicits.global

import com.naltharial.backend.BackendData.Account
import com.naltharial.backend.BackendService

trait BackendMock extends BackendService {
  override val current_exchange: String = "amq.direct"
  override val routing_key: String = ""

  def getAccount(itemId: Long): Future[Option[Account]] = Future {
    itemId match {
      case 1 => None
      case 2 => Option(Account(2, "active_account", isActive = true))
      case 3 => Option(Account(3, "inactive_account", isActive = false))
      case 4 => Option(Account(4, "active_account2", isActive = true))
      case 5 => Option(Account(5, "active_account3", isActive = true))
    }
  }
}
