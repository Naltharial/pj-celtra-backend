package com.naltharial.backend.mock

import com.naltharial.backend.actors.OutputActor


class OutputMockActor extends OutputActor {
  import OutputActor._

  var output: String = ""

  override def receive: PartialFunction[Any, Unit] = {
    case Message(msg) =>
      informWatchers(msg)
      output += msg.toString
    case Clear =>
      output = ""
    case Watch(ref) =>
      watchers += ref
    case Unwatch(ref) =>
      watchers -= ref
    case _ =>
  }
}
