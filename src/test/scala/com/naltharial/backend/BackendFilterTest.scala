package com.naltharial.backend

import scala.concurrent.duration._
import akka.actor.{ActorRef, ActorSystem, Props}
import akka.http.scaladsl.model.{HttpMethods, HttpRequest}
import akka.pattern.ask
import akka.testkit._
import akka.util.Timeout
import com.newmotion.akka.rabbitmq.{ChannelActor, ChannelCreated, ConnectionActor, CreateChannel}
import org.scalatest.words.StartWithWord
import org.scalatest.BeforeAndAfterAll
import com.naltharial.backend.BackendCLI.rabbitFactory
import com.naltharial.backend.BackendPubSub.{loggingSubscriber, setupPublisher}
import com.naltharial.backend.actors.OutputActor.Watch
import com.naltharial.backend.data.FilterList
import com.naltharial.backend.kit.ActorRouteKitSystem
import com.naltharial.backend.mock.{BackendMock, OutputMockActor}


class BackendFilterTest extends ActorRouteKitSystem(ActorSystem("BackendSystem")) with BeforeAndAfterAll with BackendMock {
  implicit def systemImpl: ActorSystem = system

  implicit val timeout = Timeout(2.seconds)


  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }

  "Filter list" should {
    val connectionActor = system.actorOf(ConnectionActor.props(rabbitFactory(system)), "rabbitmq-tf")


    "allow any when empty" in {
      val tp = new TestProbe(system, "testprobe-tf1")
      implicit val om: ActorRef = system.actorOf(Props[OutputMockActor], "output-tf1")
      implicit val fl = new FilterList(new Array[Long](0), false)

      om ! Watch(tp.ref)
      var getRequest = HttpRequest(
        HttpMethods.GET,
        uri = "/2?data=test2"
      )

      (connectionActor ? CreateChannel(ChannelActor.props(setupPublisher("amq.direct", "pubsub-tf1")), Some("publisher-tf1"))).mapTo[ChannelCreated] map {
        case ChannelCreated(pbActor) =>
          (connectionActor ? CreateChannel(ChannelActor.props(loggingSubscriber("amq.direct", "pubsub-tf1")), Some("subscriber-tf1"))).mapTo[ChannelCreated] map {
            case ChannelCreated(sbActor) =>
              // Route might finish before we monitor
              Thread.sleep(500)

              getRequest ~> route(Option(pbActor), routing_key = "pubsub-tf1") ~> check {
                status.isSuccess shouldEqual true
              }
          }
      }

      val startWith = new StartWithWord()
      tp.expectMsgPF() {
        case s: String =>
          startWith.apply("Received data for id 2: 'test2'").apply(s)
        case _ =>
      }
    }

    "allow included when active" in {
      val tp = new TestProbe(system, "testprobe-tf2")
      implicit val om: ActorRef = system.actorOf(Props[OutputMockActor], "output-tf2")
      val farr = Array[Long](1, 2, 3, 4)
      implicit val fl = new FilterList(farr, true)

      om ! Watch(tp.ref)
      val getRequest = HttpRequest(
        HttpMethods.GET,
        uri = "/4?data=test4"
      )

      (connectionActor ? CreateChannel(ChannelActor.props(setupPublisher("amq.direct", "pubsub-tf2")), Some("publisher-tf2"))).mapTo[ChannelCreated] map {
        case ChannelCreated(pbActor) =>
          (connectionActor ? CreateChannel(ChannelActor.props(loggingSubscriber("amq.direct", "pubsub-tf2")), Some("subscriber-tf2"))).mapTo[ChannelCreated] map {
            case ChannelCreated(sbActor) =>
              // Route might finish before we monitor
              Thread.sleep(500)

              getRequest ~> route(Option(pbActor), routing_key = "pubsub-tf2") ~> check {
                status.isSuccess shouldEqual true
              }
          }
      }

      tp.expectMsgPF() {
        case s: String =>
          startWith.apply("Received data for id 4: 'test4'").apply(s)
        case _ =>
      }
    }

    "disallow filtered when active" in {
      val tp = new TestProbe(system, "testprobe-tf3")
      implicit val om: ActorRef = system.actorOf(Props[OutputMockActor], "output-tf3")
      val farr = Array[Long](1, 2, 3, 4)
      implicit val fl = new FilterList(farr, true)

      om ! Watch(tp.ref)
      val getRequest = HttpRequest(
        HttpMethods.GET,
        uri = "/5?data=test5"
      )

      (connectionActor ? CreateChannel(ChannelActor.props(setupPublisher("amq.direct", "pubsub-tf3")), Some("publisher-tf3"))).mapTo[ChannelCreated] map {
        case ChannelCreated(pbActor) =>
          (connectionActor ? CreateChannel(ChannelActor.props(loggingSubscriber("amq.direct", "pubsub-tf3")), Some("subscriber-tf3"))).mapTo[ChannelCreated] map {
            case ChannelCreated(sbActor) =>
              // Route might finish before we monitor
              Thread.sleep(500)

              getRequest ~> route(Option(pbActor), routing_key = "pubsub-tf3") ~> check {
                status.isSuccess shouldEqual true
              }
          }
      }

      tp.expectNoMsg(Duration(2, SECONDS))
    }
  }
}
