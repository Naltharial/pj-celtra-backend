package com.naltharial.backend

import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.MissingQueryParamRejection
import org.scalatest.{Matchers, WordSpec}
import com.naltharial.backend.mock.BackendMock


class BackendRouteTest extends WordSpec with Matchers with ScalatestRouteTest with BackendMock {
  "No data=val" should {
    "reject request" in {

      val getRequest = HttpRequest(
        HttpMethods.GET,
        uri = "/1"
      )

      getRequest ~> route(None) ~> check {
        rejection shouldEqual MissingQueryParamRejection("data")
      }
    }
  }

  "Wrong Account Id" should {
    "fail" in {

      val getRequest = HttpRequest(
        HttpMethods.GET,
        uri = "/1?data=test1"
      )

      getRequest ~> route(None) ~> check {
        status.isSuccess shouldEqual false
      }
    }
    "return 404" in {

      val getRequest = HttpRequest(
        HttpMethods.GET,
        uri = "/1?data=test1"
      )

      getRequest ~> route(None) ~> check {
        status.intValue shouldEqual 404
      }
    }
  }

  "Inactive Account Id" should {
    "fail" in {

      val getRequest = HttpRequest(
        HttpMethods.GET,
        uri = "/3?data=test3"
      )

      getRequest ~> route(None) ~> check {
        status.isSuccess shouldEqual false
      }
    }
    "return 422" in {

      val getRequest = HttpRequest(
        HttpMethods.GET,
        uri = "/3?data=test"
      )

      getRequest ~> route(None) ~> check {
        status.intValue shouldEqual 422
      }
    }
  }

  "Active Account Id" should {
    "succeed" in {

      val getRequest = HttpRequest(
        HttpMethods.GET,
        uri = "/2?data=test"
      )

      getRequest ~> route(None) ~> check {
        status.isSuccess shouldEqual true
      }
    }
    "return 200" in {

      val getRequest = HttpRequest(
        HttpMethods.GET,
        uri = "/2?data=test"
      )

      getRequest ~> route(None) ~> check {
        status.intValue shouldEqual 200
      }
    }
  }
}
