package com.naltharial.backend.kit

import akka.actor.ActorSystem
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.testkit.TestKit
import org.scalatest.{Matchers, WordSpecLike}

trait ActorRouteKit extends WordSpecLike with Matchers with ScalatestRouteTest

class ActorRouteKitSystem(override val system: ActorSystem) extends TestKit(system) with ActorRouteKit
