package com.naltharial.backend.kit

import com.naltharial.backend.BackendAPI
import com.typesafe.config.Config
import com.zaxxer.hikari.{HikariConfig, HikariDataSource}

class BackendTest extends BackendAPI {
  override def setDS(akkaConfig: Config): Unit = {
    val config = new HikariConfig
    config.setDriverClassName("com.mysql.cj.jdbc.Driver")
    config.setJdbcUrl(akkaConfig.getString("database.test.url"))
    config.setUsername(akkaConfig.getString("database.username"))
    config.setPassword(akkaConfig.getString("database.password"))

    config.addDataSourceProperty("cachePrepStmts", "true")
    config.addDataSourceProperty("prepStmtCacheSize", "250")
    config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048")
    config.addDataSourceProperty("useServerPrepStmts", "true")

    val hds = new HikariDataSource(config)

    hds.setMaximumPoolSize(20)
    hds.setConnectionTimeout(5000)
    hds.setIdleTimeout(28740000)
    hds.setMaxLifetime(28740000)

    ds = Option(hds)
  }
}
