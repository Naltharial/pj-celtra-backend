package com.naltharial.backend

import java.io.InputStream

import akka.http.scaladsl.model.{HttpMethods, HttpRequest}
import akka.testkit._

import org.scalatest.BeforeAndAfterAll

import com.naltharial.backend.kit.{ActorRouteKit, BackendTest}


class BackendDBTest extends BackendTest with ActorRouteKit with BeforeAndAfterAll {
  setDS(system.settings.config)

  val upgrade: InputStream = getClass.getResourceAsStream("/db/migration/upgrade_v1_accounts_table.sql")
  val upssql: String = scala.io.Source.fromInputStream(upgrade).mkString
  runQuery((conn) => {
    val stmt = conn.createStatement()
    stmt.executeUpdate(upssql)
    stmt.close()
  })

  override def afterAll {
    TestKit.shutdownActorSystem(system)

    val downgrade: InputStream = getClass.getResourceAsStream("/db/migration/downgrade_v1_accounts_table.sql")
    val downsql = scala.io.Source.fromInputStream(downgrade).mkString
    runQuery((conn) => {
      val stmt = conn.createStatement()
      stmt.executeUpdate(downsql)
      stmt.close()
    })
  }

  "Wrong Account Id" should {
    "fail" in {
      val getRequest = HttpRequest(
        HttpMethods.GET,
        uri = "/1234?data=test"
      )

      getRequest ~> route(None) ~> check {
        status.isSuccess shouldEqual false
      }
    }
    "return 404" in {
      val getRequest = HttpRequest(
        HttpMethods.GET,
        uri = "/1234?data=test"
      )

      getRequest ~> route(None) ~> check {
        status.intValue shouldEqual 404
      }
    }
  }

  "Inactive Account Id" should {
    runQuery((conn) => {
      val stmt = conn.createStatement()
      val res = stmt.executeUpdate("""INSERT INTO accounts (id, name, is_active)
                                     | VALUES (1, "test_account_inactive", 0)""".stripMargin)
      stmt.close()
    })

    "fail" in {
      val getRequest = HttpRequest(
        HttpMethods.GET,
        uri = "/1?data=test"
      )

      getRequest ~> route(None) ~> check {
        status.isSuccess shouldEqual false
      }
    }

    "return 422" in {
      val getRequest = HttpRequest(
        HttpMethods.GET,
        uri = "/1?data=test"
      )

      getRequest ~> route(None) ~> check {
        status.intValue shouldEqual 422
      }
    }
  }

  "Active Account Id" should {
    runQuery((conn) => {
      val stmt = conn.createStatement()
      stmt.executeUpdate(
        """INSERT INTO accounts (id, name, is_active)
          | VALUES (2, "test_account_active", 1)""".stripMargin)
      stmt.close()
    })

    "succeed" in {
      val getRequest = HttpRequest(
        HttpMethods.GET,
        uri = "/2?data=test"
      )

      getRequest ~> route(None) ~> check {
        status.isSuccess shouldEqual true
      }
    }

    "return 200" in {
      val getRequest = HttpRequest(
        HttpMethods.GET,
        uri = "/2?data=test"
      )

      getRequest ~> route(None) ~> check {
        status.intValue shouldEqual 200
      }
    }
  }
}
