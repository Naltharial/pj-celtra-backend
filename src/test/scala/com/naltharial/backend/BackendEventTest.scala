package com.naltharial.backend

import scala.concurrent.duration._

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.pattern.ask
import akka.http.scaladsl.model.{HttpMethods, HttpRequest}
import akka.testkit._
import akka.util.Timeout

import com.newmotion.akka.rabbitmq.{ChannelActor, ChannelCreated, ChannelMessage, ConnectionActor, CreateChannel}
import org.scalatest.{BeforeAndAfterAll, Matchers}
import org.scalatest.words.StartWithWord

import com.naltharial.backend.BackendCLI.rabbitFactory
import com.naltharial.backend.BackendPubSub.{loggingSubscriber, setupPublisher}
import com.naltharial.backend.actors.OutputActor.Watch
import com.naltharial.backend.data.FilterList
import com.naltharial.backend.kit.ActorRouteKitSystem
import com.naltharial.backend.mock.{BackendMock, OutputMockActor}


class BackendEventTest extends ActorRouteKitSystem(ActorSystem("BackendSystem"))
  with Matchers with BeforeAndAfterAll with BackendMock {

  implicit def systemImpl: ActorSystem = system

  implicit val om: ActorRef = system.actorOf(Props[OutputMockActor], "output-te")

  implicit val timeout = Timeout(2.seconds)


  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }

  "Actor system" should {
    "route messages" in {
      val echo = system.actorOf(TestActors.echoActorProps)
      echo.tell("hello world", testActor)
      expectMsg("hello world")
    }
  }

  "Event route" should {

    "publish Event message to RabbitMQ" in {
      val tp = new TestProbe(system, "testprobe-te1")

      val getRequest = HttpRequest(
        HttpMethods.GET,
        uri = "/2?data=test"
      )

      getRequest ~> route(Option(tp.ref)) ~> check {
        status.isSuccess shouldEqual true
      }

      tp.expectMsgClass(classOf[ChannelMessage])
    }


    "receive Event message from RabbitMQ" in {
      val tp = new TestProbe(system, "testprobe-te2")

      val getRequest = HttpRequest(
        HttpMethods.GET,
        uri = "/2?data=test"
      )

      om ! Watch(tp.ref)

      implicit val fl = new FilterList(new Array[Long](0), false)

      val connectionActor = system.actorOf(ConnectionActor.props(rabbitFactory(system)), "rabbitmq-te")
      (connectionActor ? CreateChannel(ChannelActor.props(setupPublisher("amq.direct", "pubsub-te")), Some("publisher-te"))).mapTo[ChannelCreated] map {
        case ChannelCreated(pbActor) =>
          (connectionActor ? CreateChannel(ChannelActor.props(loggingSubscriber("amq.direct", "pubsub-te")), Some("subscriber-te"))).mapTo[ChannelCreated] map {
            case ChannelCreated(sbActor) =>
              // Route might finish before we monitor
              Thread.sleep(500)

              getRequest ~> route(Option(pbActor), routing_key = "pubsub-te") ~> check {
                status.isSuccess shouldEqual true
              }
          }
      }

      val startWith = new StartWithWord()
      tp.expectMsgPF() {
        case s: String =>
          startWith.apply("Received data for id 2: 'test'").apply(s)
        case _ =>
      }
    }
  }
}
