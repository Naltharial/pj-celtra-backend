package com.naltharial.backend

import spray.json.DefaultJsonProtocol._
import spray.json.RootJsonFormat

object BackendData {
  final case class Draw()

  final case class Account(id: Long, name: String, isActive: Boolean)
  final case class Event(id: Long, timestamp: Long, data: String)

  implicit val accountFormat: RootJsonFormat[Account] = jsonFormat3(Account)
  implicit val eventFormat: RootJsonFormat[Event] = jsonFormat3(Event)
}
