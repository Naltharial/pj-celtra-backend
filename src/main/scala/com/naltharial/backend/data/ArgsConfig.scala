package com.naltharial.backend.data

import org.rogach.scallop.{ScallopConf, ScallopOption}


class ArgsConfig(arguments: Seq[String]) extends ScallopConf(arguments) {
  val aggregate: ScallopOption[Boolean] = opt[Boolean](required = false)
  val server: ScallopOption[Boolean] = opt[Boolean](required = false)
  val filter: ScallopOption[List[Int]] = trailArg[List[Int]](required = false)

  verify()
}
