package com.naltharial.backend.data

object FilterList {
  def fromArgs(filters: Option[Seq[String]]): FilterList = {
    filters match {
      case Some(fseq) => new FilterList(fseq.map(x => x.toLong), fseq.nonEmpty)
      case None => new FilterList(Seq[Long](), false)
    }
  }
  def fromOpts(filters: Option[Seq[Int]]): FilterList = {
    filters match {
      case Some(fseq) => new FilterList(fseq.map(x => x.toLong), fseq.nonEmpty)
      case None => new FilterList(Seq[Long](), false)
    }
  }
}

class FilterList(val filters: Seq[Long], val isValid: Boolean = true)
