package com.naltharial.backend.data

import java.io.ByteArrayOutputStream
import java.io.PrintStream
import java.nio.charset.StandardCharsets

import scala.collection.mutable

import com.inamik.text.tables._
import com.inamik.text.tables.grid._

trait DataStorage[A, B] {
  def addData(id: A, data: B): Unit
}

trait Drawable {
  def draw: String
}

class DataStore[A, B] extends mutable.HashMap[A, mutable.HashMap[B, Long]] with DataStorage[A, B] with Drawable {
  def addData(id: A, data: B): Unit = {
    val e = getOrElseUpdate(id, new mutable.HashMap[B, Long]())
    val f = e.getOrElseUpdate(data, 0)
    e.put(data, f + 1)
  }

  def draw: String = {
    var s: SimpleTable = SimpleTable.of()
      .nextRow()
      .nextCell()
      .addLine("Account ID  ")
      .nextCell()
      .addLine("Data")
      .nextRow()
      .nextRow()
      .nextCell()
    this.foreach(a => {
      s = s.addLine(a._1.toString)
      a._2.foreach(b => {
        s = s.nextCell()
          .addLine(b._1.toString + "  ")
          .nextCell()
          .addLine(b._2.toString)
          .nextRow()
          .nextRow()
          .nextCell()
      })
    })

    val baos = new ByteArrayOutputStream
    val ps = new PrintStream(baos)

    var g = s.toGrid
    g = Border.of(Border.Chars.of('+', '-', '|')).apply(g)
    Util.print(s.toGrid, ps)

    new String(baos.toByteArray, StandardCharsets.UTF_8)
  }
}
