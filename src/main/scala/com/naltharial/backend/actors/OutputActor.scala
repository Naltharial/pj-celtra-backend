package com.naltharial.backend.actors

import akka.actor.{Actor, ActorRef}

import scala.collection.mutable


object OutputActor {
  sealed trait Output
  case class Message(message: String) extends Output
  case class Watch(actor: ActorRef) extends Output
  case class Unwatch(actor: ActorRef) extends Output
  case object Clear extends Output
}
class OutputActor extends Actor {
  import OutputActor._

  val watchers: mutable.ListBuffer[ActorRef] = new mutable.ListBuffer[ActorRef]

  def informWatchers(msg: String): Unit = {
    for (watcher <- watchers) {
      watcher forward msg
    }
  }

  def receive: PartialFunction[Any, Unit] = {
    case Message(msg) =>
      informWatchers(msg)
      Console.println(msg)
    case Watch(ref) =>
      watchers += ref
    case Unwatch(ref) =>
      watchers -= ref
    case Clear =>
      Console.print("\u001B[2J\u001B[0\u003B0H")
    case _ =>
  }
}
