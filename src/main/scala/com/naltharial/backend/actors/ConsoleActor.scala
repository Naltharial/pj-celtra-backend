package com.naltharial.backend.actors

import akka.actor.{Actor, ActorRef}
import com.naltharial.backend.BackendData.Draw
import com.naltharial.backend.actors.OutputActor.{Clear, Message}
import com.naltharial.backend.data.{Drawable, FilterList}

class ConsoleActor()(implicit fl: FilterList, ds: Drawable, om: ActorRef) extends Actor {
  def receive: PartialFunction[Any, Unit] = {
    case Draw =>
      val filters = if (fl.isValid) fl.filters.mkString(", ") else "ALL"
      val msg = ds.draw

      om ! Clear
      om ! Message(f"Messages [accounts: $filters%s ]:")
      om ! Message(msg)
  }
}
