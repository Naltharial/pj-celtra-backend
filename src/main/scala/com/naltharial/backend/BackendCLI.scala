package com.naltharial.backend

import scala.io.StdIn
import scala.concurrent.duration._
import akka.actor.{ActorSystem, Props}
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import com.newmotion.akka.rabbitmq._
import com.typesafe.config.{ConfigFactory, ConfigValueFactory}
import com.naltharial.backend.actors.{ConsoleActor, OutputActor}
import com.naltharial.backend.data.{ArgsConfig, DataStore, FilterList}
import com.naltharial.backend.BackendData._
import com.naltharial.backend.BackendPubSub._
import com.naltharial.backend.actors.OutputActor.Message

object BackendCLI {
  def rabbitFactory(system: ActorSystem): ConnectionFactory = {
    val factory = new ConnectionFactory()
    factory.setHost(system.settings.config.getString("amqp.host"))
    factory.setVirtualHost(system.settings.config.getString("amqp.vhost"))
    factory.setUsername(system.settings.config.getString("amqp.username"))
    factory.setPassword(system.settings.config.getString("amqp.password"))

    factory
  }

  def main(args: Array[String]) {
    val pargs = new ArgsConfig(args)

    implicit val eventMap = new DataStore[Long, String]()

    val config = ConfigFactory.load()
      .withValue("akka.loglevel", ConfigValueFactory.fromAnyRef("OFF"))
      .withValue("akka.stdout-loglevel", ConfigValueFactory.fromAnyRef("OFF"))

    implicit val system = ActorSystem("BackendCLISystem", config)
    implicit val materializer = ActorMaterializer()
    implicit val executionContext = system.dispatcher

    val hostname = system.settings.config.getString("http.interface")
    val port = system.settings.config.getInt("http.port")

    val backendAPI = new BackendAPI
    backendAPI.setDS(system.settings.config)

    implicit val om = system.actorOf(Props[OutputActor], "output")

    val connection = system.actorOf(ConnectionActor.props(rabbitFactory(system)), "rabbitmq")

    if (pargs.aggregate.isSupplied) {
      implicit val filterVals = FilterList.fromOpts(pargs.filter.toOption)
      connection ! CreateChannel(ChannelActor.props(aggregateSubscriber()), Some("subscriber"))

      val consoleout = system.actorOf(Props(new ConsoleActor()), "consoleout")
      system.scheduler.schedule(1000.milliseconds, 500.milliseconds) {
        consoleout ! Draw
      }
    }
    else {
      implicit val filterVals = FilterList.fromOpts(pargs.filter.toOption)
      connection ! CreateChannel(ChannelActor.props(loggingSubscriber()), Some("subscriber"))
    }

    if (pargs.server.isSupplied) {
      connection ! CreateChannel(ChannelActor.props(setupPublisher()), Some("publisher"))
      val publisher = system.actorSelection("/user/rabbitmq/publisher")

      val bindingFuture = Http().bindAndHandle(backendAPI.route(Option(publisher)), hostname, port)
      om ! Message(f"Server online at http://$hostname%s:$port%d/\nPress RETURN to stop ...")

      StdIn.readLine()

      bindingFuture
        .flatMap(_.unbind())
        .onComplete(_ ⇒ system.terminate())
    }
    else {
      om ! Message(f"Connected to server\nPress RETURN to stop ...")

      StdIn.readLine()

      system.terminate()
    }
  }
}
