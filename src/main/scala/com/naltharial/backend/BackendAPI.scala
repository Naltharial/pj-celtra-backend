package com.naltharial.backend

import java.sql.{Connection, SQLException}

import scala.concurrent._
import ExecutionContext.Implicits.global
import akka.actor.{ActorRef, ActorSystem, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{HttpResponse, StatusCodes}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.stream.ActorMaterializer
import com.newmotion.akka.rabbitmq.{Channel, ChannelActor, ChannelMessage, ConnectionActor, CreateChannel}
import com.typesafe.config.{Config, ConfigFactory, ConfigValueFactory}
import com.zaxxer.hikari._
import com.github.nscala_time.time.Imports._
import com.naltharial.backend.BackendCLI.rabbitFactory
import com.naltharial.backend.BackendData._
import com.naltharial.backend.BackendPubSub._
import com.naltharial.backend.actors.{ConsoleActor, OutputActor}
import com.naltharial.backend.actors.OutputActor.Message
import com.naltharial.backend.data.{DataStore, FilterList}

import scala.io.StdIn

trait AccountResolver {
  def getAccount(itemId: Long): Future[Option[Account]]
}

trait BackendService extends AccountResolver {
  val current_exchange: String = exchange
  val routing_key: String = ""

  // catch any objects you can message - optionally
  def route(publisher: Option[{ def tell(message: Any, sender: ActorRef): Unit }],
            exchange: String = current_exchange, routing_key: String = routing_key): Route =
    get {
      parameters('data.as[String]) { data =>
        pathPrefix(LongNumber) { id =>
          val maybeItem: Future[Option[Account]] = getAccount(id)

          onSuccess(maybeItem) {
            case Some(item) =>
              if (!item.isActive)
                complete(HttpResponse(StatusCodes.UnprocessableEntity,
                  entity = f"Account ${item.id}%d is not active"))
              else {
                def publish(channel: Channel) {
                  val event = Event(item.id, DateTime.now(DateTimeZone.UTC).getMillis, data)
                  val msg = eventFormat.write(event)
                  channel.basicPublish(exchange, routing_key, null, toBytes(msg.toString()))
                }

                publisher match {
                  case Some(ar) => ar.tell(ChannelMessage(publish, dropIfNoChannel = false), ActorRef.noSender)
                  case None =>
                }

                complete(item)
              }
            case None => complete(HttpResponse(StatusCodes.NotFound,
              entity = f"Account $id%d not found"))
          }
        }
      }
    }
}

object BackendAPI {
  def main(args: Array[String]) {
    implicit val eventMap = new DataStore[Long, String]()

    val config = ConfigFactory.load()
      .withValue("akka.loglevel", ConfigValueFactory.fromAnyRef("OFF"))
      .withValue("akka.stdout-loglevel", ConfigValueFactory.fromAnyRef("OFF"))

    implicit val system = ActorSystem("BackendCLISystem", config)
    implicit val materializer = ActorMaterializer()
    implicit val executionContext = system.dispatcher

    val hostname: String = system.settings.config.getString("http.interface")
    val port: Int = system.settings.config.getInt("http.port")

    val backendAPI = new BackendAPI
    backendAPI.setDS(system.settings.config)

    implicit val om: ActorRef = system.actorOf(Props[OutputActor], "output")

    val connection = system.actorOf(ConnectionActor.props(rabbitFactory(system)), "rabbitmq")
    connection ! CreateChannel(ChannelActor.props(setupPublisher()), Some("publisher"))
    val publisher = system.actorSelection("/user/rabbitmq/publisher")

    val bindingFuture = Http().bindAndHandle(backendAPI.route(Option(publisher)), hostname, port)
    om ! Message(f"Server online at http://$hostname%s:$port%d/\nPress RETURN to stop...")

    StdIn.readLine()

    bindingFuture
      .flatMap(_.unbind())
      .onComplete(_ ⇒ system.terminate())
  }
}
class BackendAPI extends BackendService {
  var ds: Option[HikariDataSource] = None

  def setDS(akkaConfig: Config): Unit = {
    val config = new HikariConfig
    config.setDriverClassName("com.mysql.cj.jdbc.Driver")
    config.setJdbcUrl(akkaConfig.getString("database.url"))
    config.setUsername(akkaConfig.getString("database.username"))
    config.setPassword(akkaConfig.getString("database.password"))

    config.addDataSourceProperty("cachePrepStmts", "true")
    config.addDataSourceProperty("prepStmtCacheSize", "250")
    config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048")
    config.addDataSourceProperty("useServerPrepStmts", "true")

    val hds = new HikariDataSource(config)

    hds.setMaximumPoolSize(20)
    hds.setConnectionTimeout(5000)
    hds.setIdleTimeout(28740000)
    hds.setMaxLifetime(28740000)

    ds = Option(hds)
  }

  def runQuery[T](handler: Connection => T): Option[T] = {
    ds match {
      case Some(hikariDataSource) =>
        val connection: Connection = hikariDataSource.getConnection
        try {
          val result = handler(connection)
          Option(result)
        } catch {
          case e: SQLException =>
            None
          case e: Throwable =>
            e.printStackTrace()
            None
        } finally {
          connection.close()
        }
      case _ => None
    }
  }

  def getAccount(itemId: Long): Future[Option[Account]] = Future {
    runQuery[Account]((conn) => {
      val statement = conn.prepareStatement("SELECT id, name, is_active FROM accounts WHERE id = ?")
      statement.setLong(1, itemId)

      val resultSet = statement.executeQuery
      val success = resultSet.absolute(1)
      statement.close()

      Account(resultSet.getLong(1), resultSet.getString(2), resultSet.getBoolean(3))
    })
  }
}
