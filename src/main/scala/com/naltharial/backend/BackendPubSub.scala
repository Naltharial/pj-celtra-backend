package com.naltharial.backend

import akka.actor.ActorRef
import com.newmotion.akka.rabbitmq._
import com.newmotion.akka.rabbitmq.Channel
import spray.json._
import com.github.nscala_time.time.Imports._
import com.naltharial.backend.BackendData._
import com.naltharial.backend.actors.OutputActor.Message
import com.naltharial.backend.data.{DataStorage, FilterList}


object BackendPubSub {
  val exchange = "amq.fanout"
  val routing_key = ""

  def fromBytes(x: Array[Byte]) = new String(x, "UTF-8")
  def toBytes(x: String): Array[Byte] = x.getBytes("UTF-8")

  private[this] val bps = new BackendPubSub()

  def setupPublisher(exchange: String = exchange, routing_key: String = routing_key): (Channel, ActorRef) => Unit = {
    bps.setupPublisher(exchange, routing_key)
  }

  def aggregateSubscriber(exchange: String = exchange, routing_key: String = routing_key)(implicit fl: FilterList, ds: DataStorage[Long, String]): (Channel, ActorRef) => Unit = {
    bps.aggregateSubscriber(exchange, routing_key)
  }

  def loggingSubscriber(exchange: String = exchange, routing_key: String = routing_key)(implicit fl: FilterList, om: ActorRef): (Channel, ActorRef) => Unit = {
    bps.loggingSubscriber(exchange, routing_key)
  }
}
class BackendPubSub {
  import BackendPubSub._
  def setupPublisher(exchange: String, routing_key: String)(channel: Channel, self: ActorRef) {
    val queue = channel.queueDeclare().getQueue
    channel.queueBind(queue, exchange, "")
  }

  def aggregateSubscriber(exchange: String, routing_key: String)(channel: Channel, self: ActorRef)(implicit fl: FilterList, ds: DataStorage[Long, String]) {
    val queue = channel.queueDeclare().getQueue
    channel.queueBind(queue, exchange, routing_key)

    val consumer = new DefaultConsumer(channel) {
      override def handleDelivery(consumerTag: String, envelope: Envelope, properties: BasicProperties, body: Array[Byte]) {
        val msg = fromBytes(body).parseJson
        val event = msg.convertTo[Event]

        if (!fl.isValid || fl.filters.contains(event.id)) ds.addData(event.id, event.data)
      }
    }
    channel.basicConsume(queue, true, consumer)
  }

  def loggingSubscriber(exchange: String, routing_key: String)(channel: Channel, self: ActorRef)(implicit fl: FilterList, om: ActorRef) {
    val queue = channel.queueDeclare().getQueue
    channel.queueBind(queue, exchange, routing_key)

    val consumer = new DefaultConsumer(channel) {
      override def handleDelivery(consumerTag: String, envelope: Envelope, properties: BasicProperties, body: Array[Byte]) {
        val msg = fromBytes(body).parseJson
        val event = msg.convertTo[Event]
        val time = event.timestamp.toDateTime

        if (!fl.isValid || fl.filters.contains(event.id))
          om ! Message(f"Received data for id ${event.id}%d: '${event.data}%s' ($time%s)")
      }
    }
    channel.basicConsume(queue, true, consumer)
  }
}
